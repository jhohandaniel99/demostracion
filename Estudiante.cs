﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class Estudiante:Persona
    {
        public string Matricula { set; get; }
        public double Calificaciones { set; get; }

        public Estudiante()
        {
        }

        public Estudiante(string cedula ,string nombres,string apellido,int edad,string matricula, double calificaciones) : base(cedula, nombres, apellido, edad)
        {
            Matricula = matricula;
            Calificaciones = calificaciones;
        }
        public override void Leer()
        {
            base.Leer();
            Console.WriteLine("ingrese Matricula y Calificaciones"); 
            Matricula = Console.ReadLine();
            Calificaciones = Convert.ToInt32(Console.ReadLine());
        }
        public override void Mostrar()
        {
            base.Mostrar();
            Console.WriteLine(Matricula + " " + Calificaciones);
        }
    }
}
