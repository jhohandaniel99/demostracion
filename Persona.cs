﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class Persona
    {
        public string Cedula { set; get; }
        public string Nombres { set; get; }
        public string Apellido { set; get; }
        public int Edad { set; get; }
        //constructor
        public Persona()
        {
            Cedula = "";
            Nombres = "";
            Apellido = "";
            Edad = 0;
        }
        public Persona(string cedula, string nombres, string apellido, int edad)
        {
            Cedula = cedula;
            Nombres = nombres;
            Apellido = apellido;
            Edad = edad;
        }
        public virtual void Leer()
        {
            Console.WriteLine("ingrese Cedula,Nombres;apellidos,edad");
            Cedula = Console.ReadLine();
            Nombres = Console.ReadLine();
            Apellido = Console.ReadLine();
            Edad = Convert.ToInt32(Console.ReadLine());
        }
        public virtual void Mostrar()
        {
            Console.WriteLine("Los datos son:");
            Console.WriteLine(Cedula + " " + Nombres + " " + Apellido + " " + Edad);
        }
    }
}
