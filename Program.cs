﻿using System;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //objetos
            Docente docente = new Docente();
            Console.WriteLine("INGRESE DATOS DOCENTE");
            docente.Leer();
            docente.Mostrar();
            Console.WriteLine("INGRESE DATOS ESTUDIANTE");
            Estudiante estudiante = new Estudiante();
            estudiante.Leer();
            estudiante.Mostrar();
            Console.ReadKey();
        }
    }
}
