﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    //herencia ->
    class Docente:Persona
    {
        public string Contrato { set; get; }
        public string Materias { set; get; }
        public Docente()
        {
        }
        public Docente(string cedula ,string nombres,string apellido,int edad,string contrato, string materia):base (cedula,nombres,apellido,edad)
        {
            Contrato = contrato;
            Materias = materia;
        }

        public  override void Leer()
        {
            base.Leer();
            Console.WriteLine("ingrese contrato y materias");     
            Contrato = Console.ReadLine();
            Materias = Console.ReadLine();
        }
        public override void Mostrar()
        {
            base.Mostrar();
            Console.WriteLine(Contrato + " " + Materias);
        }

    }
}
